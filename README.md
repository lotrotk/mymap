## Compilation

cargo build --release

## Usage
Execute a series of commands for earch word in stdin.

Let's say you wish to display and copy every txt file to a directory dst/ :

```bash
find . -name "*.txt" | mymap "echo going to copy {}" "cp {} dst/"
```

You can also index the words if you wish to handle multiple words at a time:
```bash
find . -name "*.txt" | mymap "echo going to copy {0} to {1}" "cp {0} {1}"
```

There are also the special commands "pushd" and "popd":
```bash
ls | mymap "push {}" "echo entered subdirectory {}, displaying contents:" "ls"
```
"pushd" works with both absolute and relative paths.

Return error codes:
```rust
ErrorCode::InternalError => 1,
ErrorCode::FailedToReadStdIn => 2,
ErrorCode::NotEnoughArgsOnStdIn => 3,
ErrorCode::PushDInvalidPath => 4,
ErrorCode::PopDEmptyStack => 5,
ErrorCode::FailedToExecuteCommand => 6,
Error::InvalidIndex => 7,
```

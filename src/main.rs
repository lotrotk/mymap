use std::process::{exit, Command, Stdio};

const STDIN_BUFFER_SIZE: usize = 128;

struct CommandAndArgs {
    command: String,
    args: String,
}

fn gather_command_and_args() -> Result<Vec<CommandAndArgs>, Error> {
    let count = std::env::args().count();
    if count < 2 {
        return Err(Error::InternalError(format!(
            "Expected at least one command, found {}",
            count - 1
        )));
    }
    let mut command_and_args = Vec::with_capacity(count - 1);
    let command_and_args_iterator = std::env::args().skip(1);
    for command_and_arg in command_and_args_iterator {
        let split = command_and_arg
            .find(char::is_whitespace)
            .unwrap_or_else(|| command_and_arg.len());
        if split == 0 {
            return Err(Error::InternalError("No command provided".into()));
        }
        let (command, args) = command_and_arg.split_at(split);
        command_and_args.push(CommandAndArgs {
            command: command.to_string(),
            args: args.to_string(),
        });
    }
    Ok(command_and_args)
}

pub mod replacer {
    use super::Error;

    enum Arg<'a> {
        Text(&'a str),
        Element(u32),
    }

    pub struct Replacer<'a> {
        args: Vec<Arg<'a>>,
    }

    pub trait ElementProvider {
        fn provide<'a>(&'a mut self, arg: usize) -> Result<&'a str, Error>;
    }

    impl<'a> Replacer<'a> {
        pub fn try_new(mut cmd: &'a str) -> Result<Self, Error> {
            let mut args = Vec::new();
            let mut current_index: u32 = 0;

            loop {
                if let Some(opening) = cmd.find('{') {
                    if let Some(closing) = cmd.find('}') {
                        let element = if opening + 1 == closing {
                            let index = current_index;
                            current_index += 1;
                            Arg::Element(index)
                        } else {
                            let t = &cmd[opening + 1..closing];
                            if let Ok(number) = t.parse::<u32>() {
                                Arg::Element(number)
                            } else {
                                return Err(Error::InvalidIndex(format!(
                                    "Not a valid index: {}",
                                    t
                                )));
                            }
                        };
                        let t = &cmd[..opening];
                        if !t.is_empty() {
                            args.push(Arg::Text(t));
                        }
                        args.push(element);
                        cmd = &cmd[closing + 1..];
                        continue;
                    }
                }

                if !cmd.is_empty() {
                    args.push(Arg::Text(cmd));
                }
                break;
            }
            Ok(Self { args })
        }

        pub fn to_string<P: ElementProvider>(&self, elements: &mut P) -> Result<String, Error> {
            let mut res = String::new();
            for arg in &self.args {
                let s = match arg {
                    Arg::Text(text) => text,
                    Arg::Element(number) => elements.provide(*number as usize)?,
                };
                res.push_str(s);
            }
            Ok(res)
        }
    }
}

pub mod stdinputelements {
    use super::Error;

    #[derive(Copy, Clone)]
    enum StdInBufStateLeftOversState {
        AtWhiteSpace,
        NotWhiteSpace,
    }

    #[derive(Copy, Clone)]
    enum StdInBufState {
        LeftOvers {
            begin: usize,
            end: usize,
            state: StdInBufStateLeftOversState,
        },
        NoLeftOvers,
        EOF,
    }

    pub struct StdInElements<I: std::io::Read> {
        input: I,
        elements: Vec<String>,
        buf: Vec<u8>,
        buf_state: StdInBufState,
        buf_word: Vec<u8>,
    }

    impl<I: std::io::Read> StdInElements<I> {
        pub fn new(input: I, buf_size: usize) -> Self {
            Self {
                input,
                elements: Vec::new(),
                buf: vec![0; buf_size],
                buf_state: StdInBufState::NoLeftOvers,
                buf_word: Vec::new(),
            }
        }

        pub fn reset(&mut self) -> Result<bool, Error> {
            self.elements.clear();
            self.advance()
        }

        fn advance(&mut self) -> Result<bool, Error> {
            match self.buf_state {
                StdInBufState::EOF => Ok(false),
                StdInBufState::NoLeftOvers => {
                    self.fill_buf()?;
                    self.advance()
                }
                StdInBufState::LeftOvers { begin, end, state } => self.read_buf(begin, end, state),
            }
        }

        fn fill_buf(&mut self) -> Result<(), Error> {
            match self.input.read(&mut self.buf) {
                Ok(number) if number == 0 => {
                    self.buf_state = StdInBufState::EOF;
                }
                Ok(number) => {
                    let begin = 0;
                    let end = number;
                    let state = StdInBufStateLeftOversState::AtWhiteSpace;
                    self.buf_state = StdInBufState::LeftOvers { begin, end, state };
                }
                Err(_) => return Err(Error::FailedToReadStdIn("failed to read input")),
            }
            Ok(())
        }

        fn read_buf(
            &mut self,
            begin: usize,
            end: usize,
            state: StdInBufStateLeftOversState,
        ) -> Result<bool, Error> {
            match state {
                StdInBufStateLeftOversState::AtWhiteSpace => {
                    if let Some(end_to_whitespace) = self.buf[begin..end]
                        .iter()
                        .cloned()
                        .map(|c| c as char)
                        .position(|c| !char::is_whitespace(c))
                        .map(|p| begin + p)
                    {
                        let begin = end_to_whitespace;
                        let state = StdInBufStateLeftOversState::NotWhiteSpace;
                        self.buf_state = StdInBufState::LeftOvers { begin, end, state };
                        self.read_buf(begin, end, state)
                    } else {
                        self.buf_state = StdInBufState::NoLeftOvers;
                        self.advance()
                    }
                }
                StdInBufStateLeftOversState::NotWhiteSpace => {
                    if let Some(begin_to_whitespace) = self.buf[begin..end]
                        .iter()
                        .cloned()
                        .map(|c| c as char)
                        .position(char::is_whitespace)
                        .map(|p| begin + p)
                    {
                        self.buf_word
                            .extend_from_slice(&self.buf[begin..begin_to_whitespace]);
                        let state = StdInBufStateLeftOversState::AtWhiteSpace;
                        self.buf_state = StdInBufState::LeftOvers {
                            begin: begin_to_whitespace,
                            end,
                            state,
                        };
                        if !self.buf_word.is_empty() {
                            let mut buf_word = Vec::new();
                            std::mem::swap(&mut buf_word, &mut self.buf_word);
                            let word = String::from_utf8(buf_word)
                                .map_err(|_| Error::FailedToReadStdIn("not valid utf8 format"))?;
                            self.elements.push(word);
                        }
                        Ok(true)
                    } else {
                        self.buf_word.extend_from_slice(&self.buf[begin..end]);
                        self.buf_state = StdInBufState::NoLeftOvers;
                        self.advance()
                    }
                }
            }
        }
    }

    impl<I: std::io::Read> super::replacer::ElementProvider for StdInElements<I> {
        fn provide<'a>(&'a mut self, arg: usize) -> Result<&'a str, Error> {
            if arg >= self.elements.len() {
                let number_of_words_to_parse = arg - self.elements.len() + 1;
                for _ in 0..number_of_words_to_parse {
                    match self.advance() {
                        Ok(true) => continue,
                        Ok(false) => return Err(Error::NotEnoughArgsOnStdIn),
                        Err(err) => return Err(err),
                    }
                }
            }
            Ok(self.elements.get(arg).unwrap())
        }
    }
}

fn special_command<'a>(
    command: &str,
    mut args: impl Iterator<Item = &'a str> + Clone,
    working_dir: &mut Vec<std::path::PathBuf>,
) -> Result<bool, Error> {
    if command == "pushd" && args.clone().count() == 1 {
        let mut wd = {
            let mut pb = std::path::PathBuf::new();
            pb.push(args.next().unwrap());
            pb
        };
        if !wd.is_absolute() {
            if let Some(parent_dir) = working_dir.last() {
                let mut d = parent_dir.clone();
                d.push(wd);
                wd = d;
            }
            wd = std::fs::canonicalize(wd.clone()).map_err(|_| {
                let msg = if let Some(wd) = wd.to_str() {
                    format!("invalid path {}", wd)
                } else {
                    "invalid path".into()
                };
                Error::PushDInvalidPath(msg)
            })?;
        }
        working_dir.push(wd);
        Ok(true)
    } else if command == "popd" && args.clone().next().is_none() {
        working_dir.pop().ok_or(Error::PopDEmptyStack)?;
        Ok(true)
    } else {
        Ok(false)
    }
}

fn mymain() -> Result<(), Error> {
    let command_and_args = gather_command_and_args()?;
    let mut elements = stdinputelements::StdInElements::new(std::io::stdin(), STDIN_BUFFER_SIZE);
    loop {
        match elements.reset() {
            Ok(true) => {}
            Ok(false) => break,
            Err(err) => return Err(err),
        }

        let mut replace = |s| replacer::Replacer::try_new(s)?.to_string(&mut elements);

        let mut working_dir: Vec<std::path::PathBuf> = Vec::new();
        for ca in &command_and_args {
            let command = replace(&ca.command)?;
            let args = replace(&ca.args)?;
            let args = args.split_whitespace();

            if special_command(&command, args.clone(), &mut working_dir)? {
                continue;
            }

            let mut cmd = Command::new(&command);
            if let Some(working_dir) = working_dir.last() {
                cmd.current_dir(working_dir);
            }
            cmd.args(args)
                .stdout(Stdio::inherit())
                .stderr(Stdio::inherit())
                .output()
                .map_err(|err| {
                    Error::FailedToExecuteCommand(format!(
                        "Failed to execute {}, returned with {}",
                        &command, err
                    ))
                })?;
        }
    }
    Ok(())
}

#[derive(Debug)]
pub enum Error {
    InternalError(String),
    FailedToReadStdIn(&'static str),
    NotEnoughArgsOnStdIn,
    PushDInvalidPath(String),
    PopDEmptyStack,
    FailedToExecuteCommand(String),
    InvalidIndex(String),
}

impl Error {
    fn msg(&self) -> &str {
        match self {
            Error::InternalError(reason) => &reason,
            Error::FailedToReadStdIn(reason) => reason,
            Error::NotEnoughArgsOnStdIn => "no input anymore",
            Error::PushDInvalidPath(reason) => &reason,
            Error::PopDEmptyStack => "can not popd, stack is empty",
            Error::FailedToExecuteCommand(reason) => &reason,
            Error::InvalidIndex(reason) => &reason,
        }
    }

    fn code(&self) -> i32 {
        match self {
            Error::InternalError(_) => 1,
            Error::FailedToReadStdIn(_) => 2,
            Error::NotEnoughArgsOnStdIn => 3,
            Error::PushDInvalidPath(_) => 4,
            Error::PopDEmptyStack => 5,
            Error::FailedToExecuteCommand(_) => 6,
            Error::InvalidIndex(_) => 7,
        }
    }
}

fn main() {
    if let Err(err) = mymain() {
        eprintln!("{}", err.msg());
        exit(err.code());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn stdinelements() -> Result<(), Error> {
        use replacer::ElementProvider;

        let text = " hello my\t friendly\nneighbor  ";
        let mut elements = stdinputelements::StdInElements::new(text.as_bytes(), 16);
        assert!(elements.reset()?, "returned false");
        assert_eq!(elements.provide(0)?, "hello");
        assert_eq!(elements.provide(1)?, "my");
        assert_eq!(elements.provide(2)?, "friendly");
        assert_eq!(elements.provide(3)?, "neighbor");
        assert!(elements.provide(4).is_err(), "one argument to much");
        Ok(())
    }
}
